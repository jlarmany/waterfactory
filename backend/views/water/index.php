<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Water;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\WaterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Waters');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-6">
        <h1>
            <?php
            // echo Html::encode($this->title) 
            ?>

            <?php // echo $this->render('_search', ['model' => $searchModel]) 
            ?>

        </h1>
    </div>
    <div class="col-md-6">
        <p class="text-right">
            <span class="boxhover rounded-circle">
                <a href="index.php?r=water/create" class="btn btn-success shadow-lg p-4 rounded-circle"><?= Yii::t('app', 'ສ້າງ') ?></a>
            </span>
            <?php //= Html::a(Yii::t('app', 'ສ້າງ'), ['create'], ['class' => 'btn btn-success btn-lg p-4 rounded-circle wow fadeInRight']) 
            ?>
        </p>
    </div>
</div>
<br><br>
<div class="water-index" style="display: none;">
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'image',
            'watername',
            'unit',
            'quality',
            'sellprice',
            //'factoryid',
            //'userid',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{media} {view} {update} {delete}',
                'buttons' => [
                    'media' => function ($url, $model) {
                        return Html::a(
                            '<span class="fa fa-paperclip"></span>',
                            $url,
                            [
                                'class' => 'btn-sm  btn-dark btn_crud',
                                'title' => Yii::t('app', 'Upload Images, Videos, Doc')
                            ]
                        );
                    },
                    'view' => function ($url, $model) {
                        return Html::a(
                            '<span class="fa fa-eye"></span>',
                            $url,
                            [
                                'class' => 'btn-sm  btn-success btn_crud',
                                'title' => Yii::t('app', 'View')
                            ]
                        );
                    },
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<span class="fa fa-edit"></span>',
                            $url,
                            [
                                'class' => 'btn-sm  btn-primary btn_crud',
                                'title' => Yii::t('app', 'Edit')
                            ]
                        );
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="fa fa-trash-o"></span>',
                            $url,
                            [
                                'data-method' => "post",
                                'data-confirm' => Yii::t('app', 'Are you want to delete this item.?'),
                                'class' => 'btn-sm btn-danger btn_crud',
                                'title' => Yii::t('app', 'Delete')
                            ]
                        );
                    },
                ],
            ],
        ],
    ]);
    ?>
</div>


<div class="container">
    <div class="row">
        <?php
        $waterData = Water::find()->where(['factoryid' => $_SESSION['factoryid']])->All();
        foreach ($waterData as $waterData1) {
        ?>
            <div class="col-sm-4">
                <div class="card mb-4 mt-2 shadow-sm" style="border:1px solid #dddfe2">
                    <img class="card-img-top" src="<?= Yii::$app->request->baseUrl?>/images/<?= $waterData1['image']?>" alt="Card image cap">
                    <div class="card-body boxhover">
                        <h5 class="card-title"><?= $waterData1['watername']; ?></h5>
                        <p class="card-text">
                        <div class="row">
                            <div class="col-sm-6">ຍັງເຫຼືອ</div>
                            <div class="col-sm-3 text-center"><?= number_format($waterData1['quality']) ?></div>
                            <div class="col-sm-3 text-center"><?= $waterData1['unit'] ?></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">ລາຄາຂາຍ</div>
                            <div class="col-sm-3"><?= number_format($waterData1['sellprice']) ?></div>
                            <div class="col-sm-3 text-center"><?= Yii::t('app', 'Kip') ?></div>
                        </div>
                        </p>
                        <hr>
                        <p class=" text-right">
                            <a href="index.php?r=watersell/create&waterid=<?= $waterData1['id'] ?>#" data-toggle="tooltip" data-placement="top" tilte="<?= Yii::t('app', 'Delete')?>" class="m-0 card-link btn-sm btn-primary"><span class="fa fa-shopping-basket"></span></a>
                            <a href="index.php?r=wateradd/create&waterid=<?= $waterData1['id'] ?>#" class="m-0 card-link btn-sm btn-success"><span class="fa fa-plus-circle"></span></a>
                            <a href="index.php?r=water/view&id=<?= $waterData1['id'] ?>#" class="m-0  card-link btn-sm btn-default border"><span class="fa fa-eye"></span></a>
                            <a href="index.php?r=water/update&id=<?= $waterData1['id'] ?>#" class="m-0  card-link btn-sm btn-info"><span class="fa fa-edit"></span></a>
                            <?php
                            echo Html::a(
                                '<span class="fa fa-trash"></span>',
                                ['/water/delete', 'id' => $waterData1['id']],
                                [
                                    'class' => 'card-link btn-sm btn-danger m-0 ',
                                    'title' => Yii::t('app', 'Delete'),
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => Yii::t('app', 'Are you want to delete this item.?'),
                                    ],
                                ]
                            );
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>