<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Login';
?>
<div class="site-login">
    <div class="row">
        <div class="col-md-6">
            <div class="mt-5 offset-lg-3 col-lg-6">
                <h1 class="text-primary"><?= Html::encode($this->title) ?></h1>
                <p>Please fill out the following fields to login:</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mt-5 offset-lg-2 col-lg-8 bg-white pt-5 pb-4 pr-4 pl-4 rounded shadow" style="border:2px solid #dddfe2">
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Username')])->label(false) ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Password')])->label(false) ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
