<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;

/**
 * Manage controller
 */
class ManageController extends Controller
{
    public function actionMnf($id)
    {
        $session = Yii::$app->session;
        $session->open();
        $_SESSION['factoryid'] = $id;

        return $this->render('mnf');
    }

    public function actionSwitch($id)
    {
        if($_REQUEST['id'] == "column")
        {
            $_SESSION['column'] = $_REQUEST['id'];
            return $this->render('mnf');
        }
        else{
            unset($_SESSION['column']);
            return $this->render('mnf');
        }

    }    
}
